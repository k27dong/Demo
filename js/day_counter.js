var now = new Date();

function calculate_time() {
    var running = new Date("04/03/2000 12:00:00"); // starting date
    now.setTime(now.getTime()+250);

    gap = now - running;

    whole_days = Math.floor(gap/1000/60/60/24);

    whole_hours = Math.floor(gap/1000/60/60 - (whole_days * 24));

    whole_mins = Math.floor(gap/1000/60 - (whole_days * 24 * 60) - (whole_hours * 60));
    
    whole_sec = Math.round(gap/1000 - (whole_days * 24 * 60 * 60) - (whole_hours * 60 * 60) - (whole_mins * 60));

    if(String(whole_hours).length == 1){
        whole_hours = "0" + whole_hours;
    } 

    if(String(whole_mins).length == 1) {
        whole_mins = "0" + whole_mins;
    }
    
    if(String(whole_sec).length == 1) {
        whole_sec = "0" + whole_sec;
    }
    
    document.getElementById("running_since").innerHTML = "Since: " + running;
    document.getElementById("time_loading").innerHTML = "Running  " + whole_days + " days ";
    document.getElementById("times").innerHTML = whole_hours + ":" + whole_mins + ":" + whole_sec;
}

setInterval("calculate_time()",250);