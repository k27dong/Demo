#include <iostream>
#include <cassert>
#include <cmath>
#include <random>

/*
This program is an implementation of the Millar-Rabin Primality Test.
asdasdsa
*/

int main();
bool is_probable_prime(int n, int k);
bool is_composite(int a, int d, int n, int s);

bool is_probable_prime(int n, int k) {
    assert (n > 3);
    
    int s = 1, d;
    bool status = true;
    
    if (n % 2 == 0) {
        return false;
    }
   
    int m = (n - 1) / 2;
    while (true) {  // display N - 1 in the form of (2^s)*d
        if (m % 2 == 0) {
            s++;
            m /= 2;
        }
        else {
            d = m;
            break;
        }
    }

    for (int i = 0; i < k; i++) {   // check the primality k times
        int a = 1 + (rand() % static_cast<int>(n - 1));
        if (is_composite(a, d, n, s)) {
            return false;
        }
    }
    return true;    // if the number passes all tests, it is most likely a prime number
}

// check if N is a composite number with int a
bool is_composite(int a, int d, int n, int s) {
    if ((unsigned long long int)(pow((double)a, (double)d)) % n == 1) {  // (a^d)%n
        return false;
    }

    for (int j = 0; j < s; j++) {
        if ((unsigned long long int)(pow((double)a, (pow(2.0, (double)j) * d))) % n == n - 1) {  // (a^((2^i)*d))%n
            return false;
        }
    }
    return true;    // N must be a composite number of all above fails
}

int main() {
    for (int i = 5; i < 40; i+=2) {
        std::cout << i << ": " << is_probable_prime(i, 5) << std::endl;
    }
}