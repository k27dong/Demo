/*
 * 6 variations on checking parity (even or odd)
 */

bool is_odd_1(int n) {
    if (n % 2 == 1) {
        return true;
    }
    else {
        return false;
    }
}

bool is_odd_2(int n) {
    return n % 2 == 1;
}

bool is_odd_3(int n) {
    return n % 2 == 1 || n % 2 == -1;
}

bool is_odd_4(int n) {
    return n & 2 != 0;
}

bool is_odd_5(int n)  {
    return n >> 1 << 1 != n;
}

bool is_odd_6(int n) {
    return (n & 1) == 1;
}